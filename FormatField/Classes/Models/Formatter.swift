//
//  FormatterTextField.swift
//  CardReader
//
//  Created by Артем Шляхтин on 29.08.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

class Formatter: NSObject, FormatProtocol {
    
    let symbols: [Character] = ["*", "#"]
    
    let format: String
    lazy var length: Int = {
        [unowned self] in
        return self.format.characters.count
    }()
    
    
    // MARK: - Lifecycle
    
    fileprivate override init() {
        self.format = ""
        super.init()
    }
    
    required init(format aFormat: String) {
        self.format = aFormat
        super.init()
    }
    
    
    // MARK: - Actions
    
    func formatTextField(_ textField: UITextField, toRange range: NSRange, withString string: String) -> Bool {
        let index = range.location
        if index >= length { return false }
        let isBackspace = (string.characters.count == 0)
        
        if isBackspace {
            removeCharacter(textField, toRange: range)
            return true
        }

        let character = self.character(index)
        if character == nil { return false }
        
        if isFormatContainCharacter(character!) {
            if isCanWrite(character!, string: string) {
                let _ = addCharacter(textField, toRange: range, withString: string)
            }
        } else {
            let s = Character(string)
            if s != character {
                let _ = self.formatTextField(textField, toRange: range, withString: String(character!))
                
                let newLocation = textField.text!.characters.count
                let newRange = NSMakeRange(newLocation, range.length)
                let _ = self.formatTextField(textField, toRange: newRange, withString: string)
                
                return false
            }
            
            let _ = addCharacter(textField, toRange: range, withString: string)
        }
        
        return false
    }
    
    
    // MARK: - Private
    
    fileprivate func addCharacter(_ textField: UITextField, toRange range: NSRange, withString str: String) -> Bool {
        textField.text = textField.text! + str
        
        let location = range.location+1
        let character = self.character(location)
        if character == nil { return false }
        
        if !isFormatContainCharacter(character!) {
            let newRange = NSMakeRange(location, range.length)
            let _ = self.formatTextField(textField, toRange: newRange, withString: String(character!))
        }
        return true
    }
    
    fileprivate func removeCharacter(_ textField: UITextField, toRange range: NSRange) {
        let location = range.location-1
        let character = self.character(location)
        if character == nil { return }
        
        if !isFormatContainCharacter(character!) {
            textField.text = String(textField.text!.characters.dropLast())
            let newRange = NSMakeRange(location, range.length)
            let _ = self.formatTextField(textField, toRange: newRange, withString: "")
        }
    }
    
    fileprivate func isCanWrite(_ character: Character, string: String) -> Bool {
        let isCanWriteOnlyDecimalDigit = (character == symbols[1]) && (Int(string) != nil)
        let isCanWriteAnyCharacters = (character == symbols[0])
        if (isCanWriteOnlyDecimalDigit || isCanWriteAnyCharacters) { return true }
        return false
    }

    fileprivate func isFormatContainCharacter(_ ch: Character) -> Bool {
        return symbols.contains(ch)
    }
    
    fileprivate func character(_ index: Int) -> Character? {
        if index < 0 || index >= format.characters.count { return nil }
        
        let i = format.characters.index(format.startIndex, offsetBy: index)
        let character = format[i]
        return character
    }

}
