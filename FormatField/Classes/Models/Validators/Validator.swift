//
//  ValidatorTextField.swift
//  FormatField
//
//  Created by Артем Шляхтин on 20.09.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

class Validator: NSObject {
    
    var lengthField: Int = 0
    
    
    // MARK: - Lifecycle
    
    override init() {
        super.init()
    }
    
    convenience init(lengthField aLength: Int) {
        self.init()
        self.lengthField = aLength
    }
    
    
    // MARK: - Public
    
    func verify(_ text: String?) -> Bool {
        return false
    }
    
}
