//
//  CardCodeValidator.swift
//  FormatField
//
//  Created by Артем Шляхтин on 26.09.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

class CardCodeValidator: Validator {
    
    // MARK: - Public
    
    override func verify(_ text: String?) -> Bool {
        if text?.characters.count == lengthField { return true }
        return false
    }
    
}
