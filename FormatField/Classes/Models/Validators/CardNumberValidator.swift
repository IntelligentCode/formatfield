//
//  CardNumberValidator.swift
//  FormatField
//
//  Created by Артем Шляхтин on 24/09/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

class CardNumberValidator: Validator {
    
    let numberLengths = [16, 18]
    
    // MARK: - Public
    
    override func verify(_ text: String?) -> Bool {
        let numbers = Array(self.clearFormat(text).characters)
        if !numberLengths.contains(numbers.count) { return false }
        return self.checksum(numbers)
    }
    
    
    // MARK: - Private
    
    /**
     Функция проверяет корретность номера карты с помошью алгоритма Луна
     */
    fileprivate func checksum(_ numbers: [Character]) -> Bool {
        var sum = 0
        for i in 0 ..< numbers.count {
            let s = String(numbers[i])
            var n = Int(s)!
            if (i % 2) == 0 {
                n *= 2
                if n > 9 { n -= 9 }
            }
            sum += n
        }
        
        return ((sum % 10) == 0) && sum > 0
    }
    
    fileprivate func clearFormat(_ number: String?) -> String {
        return number!.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }

}
