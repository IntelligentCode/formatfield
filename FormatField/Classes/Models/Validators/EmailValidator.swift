//
//  EmailValidator.swift
//  FormatField
//
//  Created by Артем Шляхтин on 25/09/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

class EmailValidator: Validator {
    
    // MARK: - Public

    override func verify(_ text: String?) -> Bool {
        
        if !isContainsNecessaryCharacters(text) { return false }
        if !verifyFormat(text) { return false }
        
        return true
    }
    
    
    // MARK: - Private
    
    fileprivate func isContainsNecessaryCharacters(_ text: String?) -> Bool {
        let characters = text!.characters
        
        let isExistSpace = characters.contains(" ")
        let isExistDog = characters.contains("@")
        let isExistDot = characters.contains(".")
        if isExistSpace || !isExistDog || !isExistDot { return false }
    
        return true
    }
    
    fileprivate func verifyFormat(_ text: String?) -> Bool {
        let characters = text!.characters
        if characters.first == "@" || characters.last == "@" { return false }
        if characters.first == "." || characters.last == "." { return false }
        if text!.range(of: "@.") != nil { return false }
        if text!.range(of: ".@") != nil { return false }
        
        return true
    }
    
}
