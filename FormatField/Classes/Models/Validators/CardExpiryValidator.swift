//
//  CardExpiryValidator.swift
//  FormatField
//
//  Created by Артем Шляхтин on 20.09.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
import System

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class CardExpiryValidator: Validator {
    
    let index = 2
    
    
    // MARK: - Public
    
    override func verify(_ text: String?) -> Bool {
        if text == nil { return false }
        
        let length = text?.characters.count
        if length != lengthField { return false }
        
        if !verifyMonth(text) { return false }
        if !verifyYear(text) { return false }
        
        return true
    }
    
    
    // MARK: - Private
    
    fileprivate func verifyMonth(_ text: String?) -> Bool {
        let date = Date()
        
        let currentMonth = date.month
        let rangMonth = text!.characters.index(text!.startIndex, offsetBy: index)
        let month = Int(text!.substring(to: rangMonth))
        if (month < 1 || month > 12) { return false }
        
        let currentYear = date.year % 100
        let year = extractYear(text)
        if month < currentMonth && currentYear == year { return false }
        
        return true
    }
    
    fileprivate func verifyYear(_ text: String?) -> Bool {
        let date = Date()
        let currentYear = date.year % 100
        
        let year = self.extractYear(text)
        if year < currentYear { return false }
        
        return true
    }
    
    fileprivate func extractYear(_ text: String?) -> Int? {
        let rangeYear = text!.characters.index(text!.endIndex, offsetBy: -index)
        let year = Int(text!.substring(from: rangeYear))
        return year!
    }
    
}
