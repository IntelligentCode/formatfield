//
//  Library.swift
//  FormatField
//
//  Created by Artem Salimyanov on 30.03.17.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import Foundation

public struct FormatField {
    public static let bundleName = "ru.roseurobank.FormatField"
}

protocol FormatProtocol {
    
    var format: String { get }
    var length: Int { get }
    func formatTextField(_ textField: UITextField, toRange range: NSRange, withString string: String) -> Bool
    
}
