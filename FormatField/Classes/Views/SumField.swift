//
//  SumField.swift
//  FormatField
//
//  Created by Artem Salimyanov on 30.05.17.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import UIKit

open class SumField: PlaceholderField {
    
    let defaultMask = "######"
    let decimalSeparator = ","
    var isDecimalValue: Bool = false
    
    // MARK: - Lifecycle
    
    override func performInitialization() {
        super.performInitialization()
        self.formatter = Formatter(format: defaultMask)
        self.validator = SumValidator()
        self.coonfigure()
    }
    
    // MARK: Format Text Field
    
    open override func formatTextField(_ range: NSRange, replacementText text: String) -> Bool {
        guard let currentText = self.text else { return true }
        let currentLength = currentText.characters.count
        
        if text == decimalSeparator && isDecimalValue == false {
            replaceFormatToDecimal(currentLength)
        }
        if currentLength > range.location && currentText.characters.last == decimalSeparator.characters.last {
            replaceFormatToDefault()
        }
        return super.formatTextField(range, replacementText: text)
    }
    
    fileprivate func replaceFormatToDecimal(_ n: Int) {
        var mask = ""
        for _ in 0..<n { mask = mask + "#" }
        mask = mask + "*##"
        self.formatter = Formatter(format: mask)
        isDecimalValue = true
    }
    
    fileprivate func replaceFormatToDefault() {
        self.formatter = Formatter(format: defaultMask)
        isDecimalValue = false
    }
    
    
    // MARK: - Private
    
    fileprivate func coonfigure() {
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.spellCheckingType = .no
        self.keyboardType = .decimalPad
        self.returnKeyType = .done
    }
}

