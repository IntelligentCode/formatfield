//
//  PhoneField.swift
//  FormatField
//
//  Created by Артем Шляхтин on 19.09.15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

open class PhoneField: PlaceholderField {
    
    // MARK: - Lifecycle
    
    override func performInitialization() {
        super.performInitialization()
        self.formatter = Formatter(format: "+7 (###) ###-##-##")
        self.validator = PhoneValidator(lengthField: formatter!.length)
        self.confgure()
    }
    
    
    // MARK: - Private
    
    fileprivate func confgure() {
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.spellCheckingType = .no
        self.keyboardType = .numberPad
        self.clearButtonMode = .whileEditing
        self.returnKeyType = .done
    }

}
