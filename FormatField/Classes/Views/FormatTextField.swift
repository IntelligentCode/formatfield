//
//  TemplateTextField.swift
//
//  Created by Артем Шляхтин on 28.08.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l <= r
    default:
        return !(rhs < lhs)
    }
}

@IBDesignable open class FormatTextField: UITextField {
    
    @IBInspectable var errorTextColor: UIColor {
        get { return errorColor }
        set (newColor) {
            errorColor = newColor
        }
    }
    
    var userFormat: String = ""
    
    public var isFormatFull: Bool = false
    var formatter: FormatProtocol?
    var validator: Validator?
    fileprivate var previousText: String?
    fileprivate var lengthFormat: Int = 0
    fileprivate var regularColor = UIColor.black
    fileprivate var errorColor = UIColor.red
    
    
    // MARK: - Lifecycle
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        performInitialization()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        performInitialization()
    }
    
    func performInitialization() {
        if let color = self.textColor { regularColor = color }
        addObservers()
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        configureFormatter()
        configureFormatLength()
    }
    
    // MARK: - Actions
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    open override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return !self.isFirstResponder
    }
    
    
    // MARK: - Properties
    
    @IBInspectable var customFormat: String {
        set (newFormat) { userFormat = newFormat }
        get { return userFormat }
    }
    
    open var format: String? {
        get {
            return self.formatter?.format
        }
    }
    
    // MARK: - Public
    
    open func cancel() {
        self.textColor = regularColor
        self.text = self.previousText
    }
    
    
    // MARK: - Formatter
    
    open func formatTextField(_ range: NSRange, replacementText text: String) -> Bool {
        let isVerified = self.textColor!.isEqual(errorColor)
        if isVerified { self.textColor = regularColor }
        
        if let formatterText = self.formatter {
            checkFormatIsFull(location: range.location-range.length)
            return formatterText.formatTextField(self, toRange: range, withString: text)
        }
        return true
    }
    
    private func checkFormatIsFull(location: Int) {
        if lengthFormat == 0 { return }
        self.isFormatFull = (location >= lengthFormat-1) ? true : false
    }
    
    
    // MARK: - Validator
    
    open func verify() -> Bool {
        let length = self.text?.characters.count
        if length <= 0 || validator == nil { return true }
        
        let success = self.validator!.verify(self.text)
        if !success { self.textColor = errorColor }
        return success
    }
    
    
    // MARK: - Notification
    
    fileprivate func addObservers() {
        self.addTarget(self, action: #selector(FormatTextField.textFieldDidBeginEditing), for: .editingDidBegin)
        self.addTarget(self, action: #selector(FormatTextField.textFieldDidEndEditing), for: .editingDidEnd)
    }
    
    internal dynamic func textFieldDidBeginEditing() {
        self.previousText = self.text
    }
    
    internal dynamic func textFieldDidEndEditing() {
        
    }
    
    // MARK: - Configuration
    
    fileprivate func configureFormatLength() {
        if let length = self.formatter?.length {
            lengthFormat = length
        }
    }
    
    fileprivate func configureFormatter() {
        if formatter != nil { return }
        if userFormat.characters.count == 0 { return }
        self.formatter = Formatter(format: userFormat)
        
    }
    
}
