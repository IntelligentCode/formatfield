//
//  PlaceholderTextField.swift
//  FormatField
//
//  Created by Артём Шляхтин on 02.06.16.
//  Copyright © 2016 Артем Шляхтин. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@IBDesignable open class PlaceholderField: FormatTextField {
    
    fileprivate let customPlaceholder = CATextLayer()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        performInitialization()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        performInitialization()
    }
    
    override func performInitialization() {
        super.performInitialization()
        
        if let newFont = self.font { defaultFont = newFont }
        configurationPlaceholder()
    }
    
    deinit {
    }
    
    
    // MARK: - Lifecycle
    
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if enablePlaceholder == false { return }
        customPlaceholder.frame.origin = calculatePositionPlaceholder()
        customPlaceholder.frame.size = CGSize(width: bounds.width-trailingMargin, height: defaultFont.lineHeight)
        
        if self.text?.characters.count > 0 {
            let fontSize = calculateFontSizePlaceholder()
            customPlaceholder.fontSize = fontSize
        }
    }
    
    
    // MARK: - Layout
    
    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        let textBounds = super.editingRect(forBounds: bounds)
        switch enablePlaceholder {
             case true: return CGRect(x: leadingMargin, y: customPlaceholder.frame.height, width: self.bounds.width, height: self.bounds.height-customPlaceholder.frame.height)
            case false: return textBounds
        }
    }
    
    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let textBounds = super.editingRect(forBounds: bounds)
        switch enablePlaceholder {
             case true: return CGRect(x: leadingMargin, y: customPlaceholder.frame.height, width: self.bounds.width, height: self.bounds.height-customPlaceholder.frame.height)
            case false: return textBounds
        }
    }

    
    // MARK: - Configuration Placeholder
    
    @IBInspectable var enablePlaceholder: Bool {
        set (value) {
            if value == false {
                self.contentVerticalAlignment = .center
                return
            }
            
            self.contentVerticalAlignment = .top
            customPlaceholder.removeFromSuperlayer()
            self.layer.addSublayer(customPlaceholder)
        }
        
        get {
            return customPlaceholder.superlayer != nil
        }
    }
    
    @IBInspectable var hint: String? {
        set (value) { customPlaceholder.string = value }
        get { return customPlaceholder.string as? String }
    }
    
    @IBInspectable var minFontSize: CGFloat = 14.0
    
    fileprivate var placeholderHeight: NSLayoutConstraint?
    fileprivate var defaultFont = UIFont.systemFont(ofSize: 14.0)
    fileprivate let leadingMargin: CGFloat = 6.0
    fileprivate let trailingMargin: CGFloat = 6.0*2
    
    fileprivate func configurationPlaceholder() {
        customPlaceholder.foregroundColor = UIColor(red: 0.0, green: 0.0, blue: 25.0/255.0, alpha: 0.22).cgColor
        customPlaceholder.font = font
        customPlaceholder.contentsScale = UIScreen.main.scale
        customPlaceholder.fontSize = defaultFont.pointSize
    }
    
    fileprivate func calculatePositionPlaceholder() -> CGPoint {
        var newPoint = CGPoint.zero
        if text?.characters.count > 0 || isEditing {
            newPoint = CGPoint(x: leadingMargin, y: 2.0)
        } else {
            newPoint = CGPoint(x: leadingMargin, y: bounds.height/2.0-defaultFont.lineHeight/2.0)
        }
        return newPoint
    }
    
    fileprivate func calculateFontSizePlaceholder() -> CGFloat {
        if text?.characters.count > 0 || isEditing { return minFontSize }
        else { return defaultFont.pointSize }
    }
    
}


// MARK: - Notification

extension PlaceholderField {
    
    override func textFieldDidBeginEditing() {
        super.textFieldDidBeginEditing()
        
        let currentPoint = customPlaceholder.frame.origin
        let newPoint = calculatePositionPlaceholder()
        customPlaceholder.frame.origin = newPoint
        
        let fontSize = calculateFontSizePlaceholder()
        customPlaceholder.fontSize = fontSize
        
        let animationSize = CABasicAnimation(keyPath: "frame.origin")
        animationSize.fromValue = NSValue(cgPoint: currentPoint)
        animationSize.toValue = NSValue(cgPoint: newPoint)
        animationSize.duration = 0.2
        customPlaceholder.add(animationSize, forKey: "animationSize")
        
        let animationFont = CABasicAnimation(keyPath: "fontSize")
        animationFont.toValue = fontSize
        animationFont.duration = 2.0
        customPlaceholder.add(animationFont, forKey: "animationFont")
        
    }
    
    override func textFieldDidEndEditing() {
        super.textFieldDidEndEditing()
        
        let currentPoint = customPlaceholder.frame.origin
        let newPoint = calculatePositionPlaceholder()
        let fontSize = calculateFontSizePlaceholder()
        customPlaceholder.frame.origin = newPoint
        customPlaceholder.fontSize = fontSize
        
        let animationSize = CABasicAnimation(keyPath: "frame.origin")
        animationSize.fromValue = NSValue(cgPoint: currentPoint)
        animationSize.toValue = NSValue(cgPoint: newPoint)
        animationSize.duration = 0.2
        customPlaceholder.add(animationSize, forKey: "animationSize")
        
        let animationFont = CABasicAnimation(keyPath: "fontSize")
        animationFont.toValue = fontSize
        animationFont.duration = 0.2
        customPlaceholder.add(animationFont, forKey: "animationFont")
    }
    
}

