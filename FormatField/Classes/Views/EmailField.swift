//
//  EmailField.swift
//  FormatField
//
//  Created by Артем Шляхтин on 25/09/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

open class EmailField: PlaceholderField {

    // MARK: - Lifecycle
    
    override func performInitialization() {
        super.performInitialization()
        self.validator = EmailValidator()
        self.confgure()
    }
    
    
    // MARK: - Private
    
    fileprivate func confgure() {
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.spellCheckingType = .no
        self.keyboardType = .emailAddress
        self.clearButtonMode = .whileEditing
        self.returnKeyType = .done
    }
    
}
