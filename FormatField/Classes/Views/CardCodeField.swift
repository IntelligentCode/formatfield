//
//  CVVTextField.swift
//  CardReader
//
//  Created by Артем Шляхтин on 01.09.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

open class CardCodeField: PlaceholderField {

    // MARK: - Lifecycle
    
    override func performInitialization() {
        super.performInitialization()
        self.formatter = Formatter(format: "###")
        self.validator = CardCodeValidator(lengthField: formatter!.length)
        self.confgure()
    }
    
    
    // MARK: - Private
    
    fileprivate func confgure() {
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.spellCheckingType = .no
        self.keyboardType = .numberPad
        self.isSecureTextEntry = true
    }
    
}
