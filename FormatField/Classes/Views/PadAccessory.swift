//
//  NumberPadToolbar.swift
//
//  Created by Артем Шляхтин on 01.09.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
import System

public enum PadAccessoryButton {
    case right
    case left
    case all
    case none
}

@objc public protocol PadAccessoryDelegate: UIToolbarDelegate {
    func padToolbarDidClickRightButton(_ textField: UITextField)
    @objc optional func padToolbarDidClickLeftButton(_ textField: UITextField)
    @objc optional func padToolbarLeftButtonName(_ textField: UITextField) -> String?
    @objc optional func padToolbarRightButtonName(_ textField: UITextField) -> String?
}

open class PadAccessory: UIToolbar {
    
    fileprivate weak var delegateNumberPad: PadAccessoryDelegate?
    fileprivate var textField: UITextField?
    
    open override var delegate: UIToolbarDelegate? {
        didSet {
            if delegate != nil {
                let castedDelegate = unsafeBitCast(delegate, to: PadAccessoryDelegate.self)
                self.delegateNumberPad = castedDelegate
            } else {
                self.delegateNumberPad = nil
            }
        }
    }
    
    
    // MARK: - Lifecycle
    
    fileprivate override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.performInitialization()
    }
    
    public convenience init(textField aTextField: UITextField, delegate: PadAccessoryDelegate) {
        self.init()
        self.textField = aTextField
        self.delegateNumberPad = delegate
        self.performInitialization()
    }
    
    
    // MARK: - Public
    
    func clickRightButton() {
        if let textFieldNumber = self.textField {
            self.delegateNumberPad?.padToolbarDidClickRightButton(textFieldNumber)
        }
    }
    
    func clickLeftButton() {
        if let textFieldNumber = self.textField {
            self.delegateNumberPad?.padToolbarDidClickLeftButton?(textFieldNumber)
        }
    }
    
    
    // MARK: - Private
    
    fileprivate func performInitialization() {
        self.sizeToFit()
        self.isTranslucent = false
        self.barTintColor = UIColor.keyboardColor()
        self.addToolbarItems()
    }
    
    fileprivate func addToolbarItems() {
        let right = createRightButton()
        let left = createLeftButton()
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        var toolbarButtons: [UIBarButtonItem]? = []
        if left.title != nil { toolbarButtons?.append(left) }
        toolbarButtons?.append(space)
        if right.title != nil { toolbarButtons?.append(right) }
        
        self.setItems(toolbarButtons, animated: false)
    }
    
    fileprivate func createRightButton() -> UIBarButtonItem {
        //TODO: Сделать проверку TextField
        let titleRightButton: String? = self.delegateNumberPad?.padToolbarRightButtonName?(textField!)
        return UIBarButtonItem(title: titleRightButton, style: UIBarButtonItemStyle.plain, target: self, action: #selector(PadAccessory.clickRightButton))
    }
    
    fileprivate func createLeftButton() -> UIBarButtonItem {
        //TODO: Сделать проверку TextField
        let titleLeftButton: String? = self.delegateNumberPad?.padToolbarLeftButtonName?(textField!)
        return UIBarButtonItem(title: titleLeftButton, style: UIBarButtonItemStyle.plain, target: self, action: #selector(PadAccessory.clickLeftButton))
    }
    
}
