//
//  CardNumberTextField.swift
//  CardReader
//
//  Created by Артем Шляхтин on 28.08.15.
//  Copyright (c) 2015 Артем Шляхтин. All rights reserved.
//

import UIKit

open class CardNumberField: PlaceholderField {
    
    // MARK: - Lifecycle
    
    override func performInitialization() {
        super.performInitialization()
        
        self.formatter = Formatter(format: "#### #### #### ####")
        self.validator = CardNumberValidator()
        self.confgure()
    }
    

    // MARK: - Format Text Field
    
    open override func formatTextField(_ range: NSRange, replacementText text: String) -> Bool {
        if self.text?.characters.count == 0 { replaceFormat(text) }
        return super.formatTextField(range, replacementText: text)
    }
    
    fileprivate func replaceFormat(_ symbol: String) {
        var mask: String!
        switch (symbol) {
            case "6": mask = "#### #### #### #### ##"
             default: mask = "#### #### #### ####"
        }
        self.formatter = Formatter(format: mask)
    }
    
    
    // MARK: - Private
    
    fileprivate func confgure() {
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.spellCheckingType = .no
        self.keyboardType = .numberPad
        self.clearButtonMode = .whileEditing
        self.leftViewMode = .always
        self.rightViewMode = .always
    }
    
}
